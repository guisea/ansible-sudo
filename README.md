guisea.sudo
=========

This role will create users and group.  A sudo configuration for the listed admin_group will be created and users added.

Requirements
------------

None

Role Variables
--------------

This role really only relies on two variables being set.  

ADMIN_GROUP which is the name of the group to be created for the admin users to belong to.

ADMIN_USERS is a list of the users to be created on the server and added to the group

The below may be entered into the vars/main.yml group_vars/main.yml in your playbook otherwise may be entered directly into the playbook as shown in the Example Playbook
    
	
	ADMIN_GROUP: admins
	ADMIN_USERS:
	  - admin_user
	  - ansible
	

Dependencies
------------

None

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      vars: 
         - ADMIN_GROUP: admins
         - ADMIN_USERS:
              - guisea
              - ansible

      roles:
         - { role: guisea.sudo }

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
